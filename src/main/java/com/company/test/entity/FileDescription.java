package com.company.test.entity;

import io.jmix.core.DeletePolicy;
import io.jmix.core.FileRef;
import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.entity.annotation.OnDeleteInverse;
import io.jmix.core.metamodel.annotation.InstanceName;
import io.jmix.core.metamodel.annotation.JmixEntity;
import jakarta.persistence.*;

import java.time.OffsetDateTime;
import java.util.UUID;

@JmixEntity
@Table(name = "FILE_DESCRIPTION", indexes = {
        @Index(name = "IDX_FILE_DESCRIPTION_PARENT", columnList = "PARENT_ID"),
        @Index(name = "IDX_FILE_DESCRIPTION_OWNER", columnList = "OWNER_ID"),
        @Index(name = "IDX_FILE_DESCRIPTION_ADDER", columnList = "ADDER_ID")
})
@Entity
public class FileDescription {
    @JmixGeneratedValue
    @Column(name = "ID", nullable = false)
    @Id
    private UUID id;

    @InstanceName
    @Column(name = "NAME")
    private String name;

    @Column(name = "TYPE_")
    private String type;

    @Column(name = "SIZE_")
    private Long size;

    @Column(name = "IS_FOLDER")
    private Boolean isFolder;

    @Column(name = "FILE_", length = 1024)
    private FileRef file;

    @Column(name = "PATH")
    private String path;

    @OnDeleteInverse(DeletePolicy.CASCADE)
    @JoinColumn(name = "PARENT_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private FileDescription parent;


    @Column(name = "LAST_MODIFIED")
    private OffsetDateTime lastModified;

    @JoinColumn(name = "ADDER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User adder;

    @Column(name = "VERSION", nullable = false)
    @Version
    private Integer version;

    public User getAdder() {
        return adder;
    }

    public void setAdder(User adder) {
        this.adder = adder;
    }

    public OffsetDateTime getLastModified() {
        return lastModified;
    }

    public void setLastModified(OffsetDateTime lastModified) {
        this.lastModified = lastModified;
    }


    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public FileDescription getParent() {
        return parent;
    }

    public void setParent(FileDescription parent) {
        this.parent = parent;
    }

    public FileRef getFile() {
        return file;
    }

    public void setFile(FileRef file) {
        this.file = file;
    }

    public Boolean getIsFolder() {
        return isFolder;
    }

    public void setIsFolder(Boolean isFolder) {
        this.isFolder = isFolder;
    }



    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}