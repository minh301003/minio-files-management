package com.company.test.view.filedescription;

import com.company.test.app.FileDescriptionLazyTreeDataGridItems;
import com.company.test.app.FileService;
import com.company.test.app.PreviewDialog;
import com.company.test.entity.FileDescription;


import com.company.test.view.main.MainView;

import com.google.common.base.Strings;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.grid.dataview.GridListDataView;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.upload.Receiver;
import com.vaadin.flow.data.provider.hierarchy.TreeData;
import com.vaadin.flow.data.provider.hierarchy.TreeDataProvider;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.renderer.Renderer;
import com.vaadin.flow.dom.Element;
import com.vaadin.flow.router.Route;
import io.jmix.core.*;
import io.jmix.flowui.Dialogs;
import io.jmix.flowui.Notifications;
import io.jmix.flowui.UiComponents;
import io.jmix.flowui.action.inputdialog.InputDialogAction;
import io.jmix.flowui.app.inputdialog.InputDialog;
import io.jmix.flowui.app.inputdialog.InputParameter;
import io.jmix.flowui.component.SupportsTypedValue;
import io.jmix.flowui.component.grid.DataGrid;
import io.jmix.flowui.component.grid.TreeDataGrid;
import io.jmix.flowui.component.textfield.TypedTextField;
import io.jmix.flowui.component.upload.FileStorageUploadField;
import io.jmix.flowui.component.upload.receiver.FileTemporaryStorageBuffer;
import io.jmix.flowui.download.Downloader;
import io.jmix.flowui.kit.action.ActionPerformedEvent;
import io.jmix.flowui.kit.action.ActionVariant;
import io.jmix.flowui.kit.component.ComponentUtils;
import io.jmix.flowui.kit.component.button.JmixButton;
import io.jmix.flowui.kit.component.upload.event.FileUploadFailedEvent;
import io.jmix.flowui.kit.component.upload.event.FileUploadFileRejectedEvent;
import io.jmix.flowui.kit.component.upload.event.FileUploadSucceededEvent;
import io.jmix.flowui.model.CollectionLoader;
import io.jmix.flowui.upload.TemporaryStorage;
import io.jmix.flowui.view.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.util.*;

@Route(value = "fileDescriptions", layout = MainView.class)
@ViewController("FileDescription.list")
@ViewDescriptor("file-description-list-view.xml")
@LookupComponent("fileDescriptionsDataGrid")
@DialogMode(width = "64em")
public class FileDescriptionListView extends StandardListView<FileDescription> {

    @ViewComponent
    private TreeDataGrid<FileDescription> fileDescriptionsDataGrid;

    @ViewComponent
    private DataGrid<FileDescription> fileDescriptionsSearchDataGrid;

    @Autowired
    protected DataManager dataManager;
    @ViewComponent
    private CollectionLoader<FileDescription> fileDescriptionsDl;

    @Autowired
    private Dialogs dialogs;

    @Autowired
    private Notifications notifications;

    @ViewComponent
    private FileStorageUploadField fileStorageUploadField;
    @Autowired
    private TemporaryStorage temporaryStorage;

    @Autowired
    protected UiComponents uiComponents;
    @ViewComponent
    private HorizontalLayout buttonsPanel;
    @ViewComponent
    private HorizontalLayout pathPanel;
    @Autowired
    private FileService fileService;
    @Autowired
    private FileDescriptionLazyTreeDataGridItems fileDescriptionLazyTreeDataGridItems;
    @ViewComponent
    private TypedTextField<String> searchField;
    @ViewComponent
    private TypedTextField<String> pathField;

    @Autowired
    private Downloader downloader;


    @Autowired
    protected FileStorageLocator fileStorageLocator;

    @Subscribe
    public void onInit(final InitEvent event) {
        //add hierarchy column
        TreeDataGrid.Column<FileDescription> nameColumn = fileDescriptionsDataGrid.addComponentHierarchyColumn(
                fileDescription -> {
                    Span span = uiComponents.create(Span.class);
                    span.setText(fileDescription.getName());
                    Icon icon;
                    if (Boolean.TRUE.equals(fileDescription.getIsFolder())){
                        icon = ComponentUtils.parseIcon("vaadin:folder");
                        icon.setColor("#ddd009");
                    } else {
                        icon = ComponentUtils.parseIcon("vaadin:file");
                        icon.setColor("#8383f7");
                    }
                    HorizontalLayout row = new HorizontalLayout(icon, span);
                    row.setAlignItems(FlexComponent.Alignment.CENTER);
                    row.setSpacing(true);
                    return row;
                }
        ).setHeader("Name").setWidth("400px");
        fileDescriptionsDataGrid.setColumnPosition(nameColumn, 0);
        //lazy loading nodes in tree data grid
        fileDescriptionsDataGrid.setDataProvider(fileDescriptionLazyTreeDataGridItems);
        //Add icon to search field
        searchField.setPrefixComponent(new Icon(VaadinIcon.SEARCH));
    }


    @Subscribe("fileDescriptionsDataGrid.create")
    public void onFileDescriptionsDataGridCreate(final ActionPerformedEvent event) {
        FileDescription fileDescription = fileDescriptionsDataGrid.getSingleSelectedItem();
        dialogs.createInputDialog(this)
                .withHeader("Enter the new folder name:")
                .withLabelsPosition(Dialogs.InputDialogBuilder.LabelsPosition.TOP)
                .withParameters(
                    InputParameter.stringParameter("parent")
                            .withLabel("Directory")
                            .withDefaultValue(
                                    fileDescription == null ? "Root" :
                                            (fileDescription.getIsFolder() ? fileDescription.getName() : fileDescription.getParent().getName())),
                    InputParameter.stringParameter("name")
                            .withLabel("Name")
                )
                .withActions(
                        InputDialogAction.action("confirm")
                                .withText("Confirm")
                                .withVariant(ActionVariant.PRIMARY)
                                .withHandler(actionEvent -> {
                                    InputDialogAction inputDialogAction = ((InputDialogAction) actionEvent.getSource());
                                    InputDialog inputDialog = inputDialogAction.getInputDialog();
                                    String folderName = inputDialog.getValue("name");
                                    // service create new folder
                                    FileDescription newFolder = fileService.createFolder(folderName, fileDescription);
                                    // add new item to data
                                    notifications.create(new Text("Create Folder " + newFolder.getName() + " successfully"))
                                            .withThemeVariant(NotificationVariant.LUMO_SUCCESS)
                                            .withDuration(5000)
                                            .show();
                                    inputDialog.closeWithDefaultAction();
                                }),
                        InputDialogAction.action("cancel")
                                .withText("Cancel")
                                .withValidationRequired(false)
                                .withHandler(actionEvent -> {
                                    InputDialogAction inputDialogAction = ((InputDialogAction) actionEvent.getSource());
                                    InputDialog inputDialog = inputDialogAction.getInputDialog();
                                    inputDialog.closeWithDefaultAction();
                                })
                )
                .open();

    }

    @Subscribe("fileStorageUploadField")
    public void onFileStorageUploadFieldFileUploadSucceeded(final FileUploadSucceededEvent<FileStorageUploadField> event) {
        FileDescription parent = fileDescriptionsDataGrid.getSingleSelectedItem();
        Receiver receiver = event.getReceiver();
        if (receiver instanceof FileTemporaryStorageBuffer) {
            UUID fileId = ((FileTemporaryStorageBuffer) receiver)
                    .getFileData().getFileInfo().getId();
            File file = temporaryStorage.getFile(fileId);
            Long fileSize = file.length();
            //Move the upload file into MinIO storage
            FileRef fileRef = temporaryStorage.putFileIntoStorage(fileId, event.getFileName());
            fileStorageUploadField.setValue(fileRef);
            //handle create File Description type File
            FileDescription uploadFile = fileService.uploadFile(fileRef, fileSize, parent);
            notifications.create(new Text("Upload File " + uploadFile.getName() + " successfully"))
                    .withThemeVariant(NotificationVariant.LUMO_SUCCESS)
                    .withDuration(5000)
                    .show();
        }
    }

    @Subscribe("fileStorageUploadField")
    public void onFileStorageUploadFieldFileUploadFailed(final FileUploadFailedEvent<FileStorageUploadField> event) {

    }


    @Subscribe("fileStorageUploadField")
    public void onFileStorageUploadFieldFileUploadFileRejected(final FileUploadFileRejectedEvent<FileStorageUploadField> event) {
        notifications.create(event.getErrorMessage())
                .withThemeVariant(NotificationVariant.LUMO_WARNING)
                .withDuration(5000)
                .show();
    }


    @Subscribe("searchField")
    protected void onValueChangeModeTextFieldValueChange(SupportsTypedValue.TypedValueChangeEvent<TypedTextField<String>, String> event) {
        TypedTextField<String> sourceTextField = event.getSource();
        sourceTextField.setHelperText(Strings.nullToEmpty(event.getValue()).length() + "/" + sourceTextField.getMaxLength());
        if (event.getValue() == null) {
            fileDescriptionsSearchDataGrid.setVisible(false);
            fileDescriptionsDataGrid.setVisible(true);
            buttonsPanel.setVisible(true);
        } else {
            buttonsPanel.setVisible(false);
            fileDescriptionsDataGrid.setVisible(false);
            fileDescriptionsSearchDataGrid.setVisible(true);
            //handle filter
            List<FileDescription> files = dataManager.load(FileDescription.class).all().list();
            GridListDataView<FileDescription> dataView = fileDescriptionsSearchDataGrid.setItems(files);
            dataView.addFilter(fileDescription -> {
                String searchTerm = event.getValue();
                return searchTerm == null || searchTerm.isEmpty()
                        || fileDescription.getName().toLowerCase().contains(searchTerm.toLowerCase());
            });
        }
    }



    @Subscribe("fileDescriptionsSearchDataGrid.goToFolder")
    public void onFileDescriptionsSearchDataGridGoToFolder(final ActionPerformedEvent event) {
        FileDescription fileDescription = fileDescriptionsSearchDataGrid.getSingleSelectedItem();
        if (fileDescription != null) {
            // back to tree data grid tab
            fileDescriptionsSearchDataGrid.setVisible(false);
            fileDescriptionsDataGrid.setVisible(true);
            buttonsPanel.setVisible(true);
            fileStorageUploadField.setVisible(true);
            // go back to folder
            TreeData<FileDescription> data = new TreeData<>();
            if (fileDescription.getParent() == null) {
                data.addItems(null, fileDescription);
            } else {
                data.addItems(null, fileDescription.getParent());
                data.addItems(fileDescription.getParent(), fileDescription);
            }
            TreeDataProvider<FileDescription> dataProvider = new TreeDataProvider<>(data);
            fileDescriptionsDataGrid.setDataProvider(dataProvider);
            fileDescriptionsDataGrid.expandRecursively(dataProvider.getTreeData().getRootItems(),
                    99);
            //get path and copy to clipboard
            fileService.setPathString(fileDescription);
            pathPanel.setVisible(true);
            pathField.setValue(fileDescription.getPath());
        }
    }

    @Subscribe(id = "copyBtn", subject = "clickListener")
    public void onCopyBtnClick(final ClickEvent<JmixButton> event) {
        Element buttonElement = event.getSource().getElement();
        String valueToCopy = Strings.nullToEmpty(pathField.getValue());
        buttonElement.executeJs(getCopyToClipboardScript(), valueToCopy)
                .then(successResult -> notifications.create("Path copied to clipboard")
                                .withThemeVariant(NotificationVariant.LUMO_SUCCESS)
                                .show(),
                        errorResult -> notifications.create("Copy failed!")
                                .withThemeVariant(NotificationVariant.LUMO_ERROR)
                                .show());
    }

    protected String getCopyToClipboardScript() {
        return """
                  const textarea = document.createElement("textarea");
                  textarea.value = $0;
                  
                  textarea.style.position = "absolute";
                  textarea.style.opacity = "0";
                  
                  document.body.appendChild(textarea);
                  textarea.select();
                  document.execCommand("copy");
                  document.body.removeChild(textarea);
                """;
    }

    @Subscribe("fileDescriptionsDataGrid.download")
    public void onFileDescriptionsDataGridDownload(final ActionPerformedEvent event) {
        FileDescription fileDescription = fileDescriptionsDataGrid.getSingleSelectedItem();
        FileRef fileRef = fileDescription.getFile();
        downloader.download(fileRef);
    }

    @Subscribe("fileDescriptionsDataGrid.rename")
    public void onFileDescriptionsDataGridRename(final ActionPerformedEvent event) {
        FileDescription fileDescription = fileDescriptionsDataGrid.getSingleSelectedItem();
        int index = fileDescription.getName().lastIndexOf('.');
        dialogs.createInputDialog(this)
                .withHeader("Rename:")
                .withLabelsPosition(Dialogs.InputDialogBuilder.LabelsPosition.TOP)
                .withParameters(
                        InputParameter.stringParameter("name")
                                .withLabel("Name")
                                .withDefaultValue(fileDescription.getName().substring(0, index))
                )
                .withActions(
                        InputDialogAction.action("confirm")
                                .withText("Confirm")
                                .withVariant(ActionVariant.PRIMARY)
                                .withHandler(actionEvent -> {
                                    InputDialogAction inputDialogAction = ((InputDialogAction) actionEvent.getSource());
                                    InputDialog inputDialog = inputDialogAction.getInputDialog();
                                    String newName = inputDialog.getValue("name");
                                    if (newName != null && !newName.equals(fileDescription.getName())) {
                                        String extension = fileDescription.getName().substring(index + 1);
                                        newName += "." + extension;
                                        fileService.renameFileOrFolder(newName, fileDescription);
                                        notifications.create(new Text("Rename successfully"))
                                                .withThemeVariant(NotificationVariant.LUMO_SUCCESS)
                                                .withDuration(5000)
                                                .show();
                                    }
                                    inputDialog.closeWithDefaultAction();

                                }),
                        InputDialogAction.action("cancel")
                                .withText("Cancel")
                                .withValidationRequired(false)
                                .withHandler(actionEvent -> {
                                    InputDialogAction inputDialogAction = ((InputDialogAction) actionEvent.getSource());
                                    InputDialog inputDialog = inputDialogAction.getInputDialog();
                                    inputDialog.closeWithDefaultAction();
                                })
                )
                .open();
    }


    @Install(to = "fileDescriptionsDataGrid.rename", subject = "enabledRule")
    private boolean fileDescriptionsDataGridRenameEnabledRule() {
        FileDescription fileDescription = fileDescriptionsDataGrid.getSingleSelectedItem();
        return (fileDescription != null);
    }

    @Subscribe("fileDescriptionsDataGrid.preview")
    public void onFileDescriptionsDataGridPreview(final ActionPerformedEvent event) {
        FileDescription fileDescription = fileDescriptionsDataGrid.getSingleSelectedItem();
        FileRef fileRef = fileDescription.getFile();
        PreviewDialog previewDialog = new PreviewDialog(fileRef, fileStorageLocator);
    }
    

    @Install(to = "fileDescriptionsDataGrid.preview", subject = "enabledRule")
    private boolean fileDescriptionsDataGridPreviewEnabledRule() {
        FileDescription fileDescription = fileDescriptionsDataGrid.getSingleSelectedItem();
        return (fileDescription != null && !fileDescription.getIsFolder());
    }

    @Subscribe("fileDescriptionsDataGrid.refresh")
    public void onFileDescriptionsDataGridRefresh(final ActionPerformedEvent event) {
        fileDescriptionsDataGrid.setDataProvider(fileDescriptionLazyTreeDataGridItems);
        pathPanel.setVisible(false);
        searchField.setValue("");
        fileDescriptionsDl.load();
    }


    @Supply(to = "fileDescriptionsSearchDataGrid.name", subject = "renderer")
    protected Renderer<FileDescription> extensionComponentRenderer() {
        return new ComponentRenderer<>(this::createExtensionComponent, this::extensionComponentUpdater);
    }

    protected HorizontalLayout createExtensionComponent() {
       HorizontalLayout row = uiComponents.create(HorizontalLayout.class);
       return row;
    }

    protected void extensionComponentUpdater(HorizontalLayout row, FileDescription fileDescription) {
        Span span = uiComponents.create(Span.class);
        span.setText(fileDescription.getName());
        Icon icon;
        if (Boolean.TRUE.equals(fileDescription.getIsFolder())){
            icon = ComponentUtils.parseIcon("vaadin:folder");
            icon.setColor("#ddd009");
        } else {
            icon = ComponentUtils.parseIcon("vaadin:file");
            icon.setColor("#8383f7");
        }
        row.add(icon, span);
        row.setAlignItems(FlexComponent.Alignment.CENTER);
        row.setSpacing(true);
    }

//    @Supply(to = "fileDescriptionsDataGrid.lastModified", subject = "renderer")
//    protected Renderer<FileDescription> dateComponentRenderer() {
//        return new ComponentRenderer<>(this::createDateComponent, this::dateComponentUpdater);
//    }
//
//    protected Span createDateComponent() {
//        Span span = uiComponents.create(Span.class);
//        return span;
//    }
//
//    protected void dateComponentUpdater(Span span, FileDescription fileDescription) {
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
//        String formattedString = fileDescription.getLastModified().format(formatter);
//        span.setText(formattedString);
//    }

}