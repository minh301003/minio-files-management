package com.company.test.app;

import com.company.test.entity.FileDescription;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.data.provider.AbstractDataProvider;
import com.vaadin.flow.data.provider.hierarchy.HierarchicalDataProvider;
import com.vaadin.flow.data.provider.hierarchy.HierarchicalQuery;
import com.vaadin.flow.shared.Registration;
import io.jmix.core.*;
import io.jmix.core.entity.EntityValues;
import io.jmix.core.metamodel.model.MetaClass;
import io.jmix.core.metamodel.model.MetaPropertyPath;
import io.jmix.flowui.Notifications;
import io.jmix.flowui.data.BindingState;
import io.jmix.flowui.data.grid.DataGridItems;
import io.jmix.flowui.kit.event.EventBus;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Stream;

@Component
public class FileDescriptionLazyTreeDataGridItems extends AbstractDataProvider<FileDescription, Void>
        implements HierarchicalDataProvider<FileDescription, Void>, DataGridItems.Sortable<FileDescription> {

    @Autowired
    protected Metadata metadata;
    @Autowired
    protected DataManager dataManager;
    @Autowired
    protected Notifications notifications;


    private FileDescription selectedItem;
    private Sort sort = Sort.UNSORTED;

    private EventBus eventBus;

    public FileDescriptionLazyTreeDataGridItems() {
    }

    @Override
    public int getChildCount(HierarchicalQuery<FileDescription, Void> query) {
        return Math.toIntExact(getItems(query).size());
    }

    @Override
    public Stream<FileDescription> fetchChildren(HierarchicalQuery<FileDescription, Void> query) {
        Collection<FileDescription> list = getItems(query);

//        notifications.create(String.format("Loaded: %d items", list.size()))
//                .withPosition(Notification.Position.TOP_CENTER)
//                .withThemeVariant(NotificationVariant.LUMO_SUCCESS)
//                .show();

        return list.stream();
    }

    protected Collection<FileDescription> getItems(HierarchicalQuery<FileDescription, Void> query) {
        return dataManager.load(FileDescription.class)
                .query("select e from FileDescription e where e.parent = :parent")
                .parameter("parent", query.getParent())
                .sort(sort)
                .firstResult(query.getOffset())
                .maxResults(query.getLimit())
                .list();
    }

    @Override
    public Collection<FileDescription> getItems() {
        return dataManager.load(FileDescription.class)
                .all()
                .sort(sort)
                .list();
    }

    @Nullable
    @Override
    public FileDescription getItem(@Nullable Object itemId) {
        return itemId == null
                ? null
                : dataManager.load(Id.of(itemId, FileDescription.class)).one();
    }

    @Override
    public Object getItemValue(Object itemId, MetaPropertyPath propertyId) {
        return EntityValues.getValueEx(getItem(itemId), propertyId);
    }

    @Override
    public FileDescription getSelectedItem() {
        return selectedItem;
    }

    @Override
    public void setSelectedItem(FileDescription item) {
        this.selectedItem = item;

        getEventBus().fireEvent(new SelectedItemChangeEvent<>(this, item));
    }


    @Override
    public void sort(Object[] propertyId, boolean[] ascending) {
        sort = createSort(propertyId, ascending);
    }

    @Override
    public void resetSortOrder() {
        sort = Sort.UNSORTED;
    }

    private Sort createSort(Object[] propertyId, boolean[] ascending) {
        List<Sort.Order> orders = new ArrayList<>();

        for (int i = 0; i < propertyId.length; i++) {
            String property;
            if (propertyId[i] instanceof MetaPropertyPath) {
                property = ((MetaPropertyPath) propertyId[i]).toPathString();
            } else {
                property = (String) propertyId[i];
            }

            Sort.Order order = ascending[i]
                    ? Sort.Order.asc(property)
                    : Sort.Order.desc(property);

            orders.add(order);
        }
        return Sort.by(orders);
    }

    @Override
    public boolean hasChildren(FileDescription item) {
        LoadContext<Object> loadContext = new LoadContext<>(getEntityMetaClass())
                .setQuery(new LoadContext.Query("select e from FileDescription e where e.parent = :parent")
                        .setParameter("parent", item));

        return dataManager.getCount(loadContext) > 0;
    }

    public MetaClass getEntityMetaClass() {
        return metadata.getClass(FileDescription.class);
    }


    @Override
    public @NotNull Registration addStateChangeListener(Consumer<StateChangeEvent> listener) {
        return getEventBus().addListener(StateChangeEvent.class, listener);
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Override
    public @NotNull Registration addValueChangeListener(Consumer<ValueChangeEvent<FileDescription>> listener) {
        return getEventBus().addListener(ValueChangeEvent.class, ((Consumer) listener));
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Override
    public @NotNull Registration addItemSetChangeListener(Consumer<ItemSetChangeEvent<FileDescription>> listener) {
        return getEventBus().addListener(ItemSetChangeEvent.class, ((Consumer) listener));
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Override
    public @NotNull Registration addSelectedItemChangeListener(Consumer<SelectedItemChangeEvent<FileDescription>> listener) {
        return getEventBus().addListener(SelectedItemChangeEvent.class, ((Consumer) listener));
    }

    @Override
    public boolean containsItem(@NotNull FileDescription item) {
        return true;
    }

    @Override
    public boolean isInMemory() {
        return true;
    }

    @Override
    public Class<FileDescription> getType() {
        return getEntityMetaClass().getJavaClass();
    }

    @Override
    public BindingState getState() {
        return BindingState.ACTIVE;
    }

    protected EventBus getEventBus() {
        if (eventBus == null) {
            eventBus = new EventBus();
        }

        return eventBus;
    }
}