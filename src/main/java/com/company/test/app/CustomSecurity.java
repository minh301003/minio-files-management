package com.company.test.app;

import io.jmix.securityflowui.FlowuiSecurityConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.HeadersConfigurer;
import org.springframework.stereotype.Component;

@Component
@EnableWebSecurity
@Configuration
public class CustomSecurity extends FlowuiSecurityConfiguration {

    @Override
    public void configure(HttpSecurity http) throws Exception {
        super.configure(http);
        http.headers(headers -> headers.frameOptions(HeadersConfigurer.FrameOptionsConfig::sameOrigin));
    }
}