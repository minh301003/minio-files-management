package com.company.test.app;

import com.company.test.pdfviewer.PdfViewer;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.server.StreamResource;
import io.jmix.core.FileRef;
import io.jmix.core.FileStorage;
import io.jmix.core.FileStorageLocator;
import java.io.*;

public class PreviewDialog extends Dialog {

    private FileRef fileRef;

    private FileStorageLocator fileStorageLocator;


    public PreviewDialog(FileRef fileRef, FileStorageLocator fileStorageLocator) {
        this.setHeight("calc(100vh - (2*var(--lumo-space-m)))");
        this.setWidth("calc(100vw - (4*var(--lumo-space-m)))");
        this.fileRef = fileRef;
        this.fileStorageLocator = fileStorageLocator;
        buildLayout();
    }

    private void buildLayout() {
        // HEADER
        HorizontalLayout header = new HorizontalLayout();
        header.setMaxHeight("1em");
        header.setAlignItems(FlexComponent.Alignment.CENTER);
        header.setJustifyContentMode(FlexComponent.JustifyContentMode.BETWEEN);
        header.getStyle().set("margin-top", "-1em");

        Span caption = new Span(getTranslation("Preview"));
        caption.getStyle().set("color", "black");
        caption.getStyle().set("font-weight", "bold");

        Icon closeIcon = new Icon(VaadinIcon.CLOSE);
        closeIcon.setColor("#000000");
        Button closeButton = new Button();
        closeButton.setIcon(closeIcon);
        closeButton.getStyle().set("border", "none");
        closeButton.getStyle().set("background", "transparent");
        closeButton.addClickListener(click -> this.close());

        header.add(caption, closeButton);
        this.add(header);

        FileStorage fileStorage = fileStorageLocator.getDefault();

        String mimeType = fileRef.getContentType();

        if (mimeType.contains("image")) {
            Image image = new Image();
            image.setSizeFull();
            StreamResource resource = new StreamResource(
                    fileRef.getFileName(),
                    () -> fileStorage.openStream(fileRef));
            image.setSrc(resource);
            this.add(image);
        } else if (mimeType.equals("application/pdf")) {
            PdfViewer pdfViewer = new PdfViewer();
            try (InputStream inputStream = fileStorage.openStream(fileRef)) {
                ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(inputStream.readAllBytes());
                StreamResource resource = new StreamResource(fileRef.getFileName(), () -> byteArrayInputStream);
                pdfViewer.setSrc(resource);
                pdfViewer.openThumbnailsView();
            } catch (IOException e) {
                throw new RuntimeException(e.getMessage());
            }
            this.add(pdfViewer);
        } else {
            Span content = new Span();
            content.setText("Can not preview this file type. Please download to view.");
            this.add(content);
        }

        this.open();

    }

}