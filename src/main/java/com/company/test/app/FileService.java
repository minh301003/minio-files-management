package com.company.test.app;

import com.company.test.entity.FileDescription;
import com.company.test.entity.User;
import io.jmix.core.DataManager;
import io.jmix.core.FileRef;
import io.jmix.core.querycondition.LogicalCondition;
import io.jmix.core.querycondition.PropertyCondition;
import io.jmix.core.security.CurrentAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.time.OffsetDateTime;
import java.util.List;

@Component
public class FileService {

    @Autowired
    private CurrentAuthentication currentAuthentication;


    private final DataManager dataManager;
    public FileService(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    public FileDescription createFolder(String folderName, FileDescription parent) {
        if (folderName == null) {
            folderName = "New Folder";
        }
        String uniqueFolderName = generateUniqueName(folderName, parent);
        FileDescription newFolder = dataManager.create(FileDescription.class);
        newFolder.setName(uniqueFolderName);
        newFolder.setIsFolder(true);
        newFolder.setParent(parent);
        newFolder.setLastModified(OffsetDateTime.now());
        UserDetails user = currentAuthentication.getUser();
        User currentUser = dataManager.load(User.class).query("e.username like ?1", user.getUsername()).one();
        newFolder.setAdder(currentUser);
        return dataManager.save(newFolder);
    }

    private String generateUniqueName(String baseName, FileDescription parent) {
        String uniqueName = baseName;
        int counter = 1;

        while (checkExisted(uniqueName, parent)) {
            uniqueName = baseName + " (" + counter + ")";
            counter++;
        }

        return uniqueName;
    }

    private boolean checkExisted(String name, FileDescription parent) {
        boolean check = false;
        List<FileDescription> existedList = dataManager.load(FileDescription.class)
                .condition(
                        LogicalCondition.and(
                                PropertyCondition.equal("name", name),
                                PropertyCondition.equal("parent", parent)
                        )
                )
                .list();
        if (!existedList.isEmpty()) {
            check = true;
        }
        return check;
    }


    public FileDescription renameFileOrFolder (String newName, FileDescription fileDescription) {
        String newUniqueName = generateUniqueName(newName, fileDescription.getParent());
        fileDescription.setName(newUniqueName);
        fileDescription.setLastModified(OffsetDateTime.now());
        return dataManager.save(fileDescription);
    }

    public FileDescription uploadFile (FileRef fileRef, Long fileSize, FileDescription parent) {
        //User select a file in a folder
        if (parent != null && !parent.getIsFolder()) {
            parent = parent.getParent();
        }
        FileDescription upLoadFile = dataManager.create(FileDescription.class);
        String uniqueName = generateUniqueName(fileRef.getFileName(), parent);
        upLoadFile.setName(uniqueName);
        upLoadFile.setFile(fileRef);
        upLoadFile.setType(fileRef.getContentType());
        upLoadFile.setSize(fileSize);
        upLoadFile.setIsFolder(false);
        upLoadFile.setParent(parent);
        upLoadFile.setLastModified(OffsetDateTime.now());
        UserDetails user = currentAuthentication.getUser();
        User currentUser = dataManager.load(User.class).query("e.username like ?1", user.getUsername()).one();
        upLoadFile.setAdder(currentUser);
        return dataManager.save(upLoadFile);
    }

    public void setPathString (FileDescription fileDescription) {
        String rootPath = "Root / ";
        if (fileDescription.getParent() == null) {
            fileDescription.setPath(rootPath + fileDescription.getName());
        } else {
            setPathString(fileDescription.getParent());
            fileDescription.setPath(fileDescription.getParent().getPath() + " / " + fileDescription.getName());
        }
    }





}