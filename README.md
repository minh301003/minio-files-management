## Launch MinIO Server
```bash
export MINIO_CONFIG_ENV_FILE=/etc/default/minio
minio server --console-address :9001
```
## Start Server
```bash
minio server start
```